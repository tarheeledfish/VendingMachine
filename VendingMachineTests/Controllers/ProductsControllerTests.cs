﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http;
using VendingMachine.Models;
using VendingMachine.Controllers;
using VendingMachine.Repositories;

namespace VendingMachine.Controllers.Tests
{
    [TestClass()]
    public class ProductsControllerTests
    {
        [TestMethod()]
        public void GetAllProductsTest()
        {
            // Arrange
            var c = new ProductsController();
            var products = c.GetAllProducts();
            var compare = "";// = new IEnumerable<Product>();

            // Act


            // Assert
            CollectionAssert.AreNotEqual(products.ToList(), compare.ToList(), "...");
        }
    }
}
