﻿var VendingMachine = {
    buffer: "",
    products: [],
    sum: 0,
    bank: [],
    lastAction: null,
    loadProducts: function(){
        fetch("api/products")
            .then(function (response) {
                return response.json();
            })
            .then(function (products) {
                VendingMachine.products = products;
                $("#Products").empty();
                $("#Products").html(
                    (
                        "Id".th()
                        + "Price".th()
                        + "Quantity".th()
                        + "Product Name".th()
                    ).row());
                $.map(products, function (product) {
                    $("#Products").append((
                        product.Id.toString().cell()
                        + formatCurrency(product.Price).cell()
                        + product.Quantity.toString().cell()
                        + product.Name.cell()
                    ).row());
                });
            });
    },
    sendPurchaseToServer: function (params) {
        fetch('api/products/' + params.Id + '/buy', 
            {
                headers: {
                    'Content-type': 'application/json',
                    'Accept': 'application/json',
                },
                method: 'POST',
                body: JSON.stringify(params),
            })
            .then(function (response) {
                return response.json();
            })
            .then(function (response) {
                // Update view.
                VendingMachine.appendStatus(response.Msg);
                VendingMachine.loadProducts();
            });
    },
    resetProducts: function(){
        fetch("api/reset")
            .then(function (response) {
                return response.json();
            })
            .then(function (response) {
                VendingMachine.reportStatus(response.Msg);
                VendingMachine.loadProducts();
            });
    },
    addToBuffer: function (number) {
        this.buffer += number;
        $("#Buffer").html(this.buffer);
        $("#Status").empty();
    },
    getDepositValue: function () {
        this.sum = 0;
        this.bank.forEach(function (denom) {
            this.sum += parseFloat(denom);
        }, this);
        return this.sum.toFixed(2);
    },
    emptyBuffer: function(){
        this.buffer = "";
        $("#Buffer").empty();
        $("#creditCardNumber").val("");
    },
    resetState: function()
    {
        this.bank = [];
        this.emptyBuffer();
    },
    returnMoney: function (msg) {
        msg = typeof msg === "undefined" ? "" : msg + "<br />";
        this.resetState();
        $("#Credit").html("$0.00");
        if (this.lastAction === "Clear")
        {
            this.reportStatus(msg + "Your unspent money has been returned.");
        } else {
            this.appendStatus(msg + "Your unspent money has been returned.");
        }
    },
    addMoney: function(money){
        var amount = money.substring(1);
        this.bank.push(amount);
        var credit = this.getDepositValue();
        $("#Credit").html("$" + credit);
        this.reportStatus("You have put $" + credit + " in this machine.");
    },
    makeChange: function (change) {
        // Multiply everything by 100 to operate on integers and avoid
        // pesky subtraction errors with decimals in JavaScript.
        change *= 100;
        var denoms = [5 * 100, 1 * 100, .25 * 100, .1 * 100, .05 * 100];
        var changes = [];
        for (var i = 0; i < denoms.length; i++)
        {
            if (change < denoms[i])
            {
                continue;
            }
            while (change > 0)
            {
                if (change < denoms[i])
                {
                    break;
                }
                change -= denoms[i].toFixed(2);
                console.log("change is now " + change);
                changes.push(denoms[i] / 100);
                console.log("changes is now " + changes);
            }
        }
        console.log(changes);
        return changes;
    },
    payWithCash: function(item){
        var deposit = this.getDepositValue();
        if (deposit >= item.Price)
        {
            var change = deposit - item.Price;
            var changeList = this.makeChange(change);
            this.sendPurchaseToServer({
                Id: item.Id,
                ccNumber: $("#creditCardNumber").val(),
                Money: this.getDepositValue(),
                PaymentType: 'cash'
            });
            this.reportStatus("Purchase successful!  Change due: $" + change.toFixed(2)
                + ".<br />The machine returned denominations of<br />"
                + changeList.map(function (item) {
                    return "$" + item
                }).join(", ")
                + ".");
            this.returnMoney();
            this.emptyBuffer();
        } else {
            var difference = (item.Price - this.getDepositValue()).toFixed(2);
            this.reportStatus(item.Id + " - " + item.Name + " costs $" + item.Price.toFixed(2)
                + ".<br />Please insert $" + difference + " more.");
            this.emptyBuffer();
        }
    },
    reportStatus: function (msg) {
        $("#Status").html(msg);
    },
    appendStatus: function (msg) {
        $("#Status").append("<br />" + msg);
    },
    validCreditCard: function (ccNumber) {
        return ccNumber.length === 16 && !isNaN(ccNumber);
    },
    payWithCreditCard: function(item){
        var ccNumber = $("#creditCardNumber").val();
        if(this.validCreditCard(ccNumber))
        {
            this.sendPurchaseToServer({
                Id: item.Id,
                ccNumber: $("#creditCardNumber").val(),
                Money: this.getDepositValue(),
                PaymentType: 'creditCard'
            });
            this.returnMoney("Paying with credit card.");
        } else {
            this.reportStatus("Invalid credit card number.")
        }
    },
    purchaseItem: function (index) {
        var item = this.getItemById(parseInt(this.buffer));

        if (typeof item === "undefined")
        {
            this.reportStatus("There is no item with that Id.");
            this.emptyBuffer();
        } else if (item.Quantity === 0) {
            this.reportStatus(item.Name + " is sold out.");
            this.emptyBuffer();
        }else{
            var paymentType = $("input[name=paymentType]:checked").val();
            if (paymentType === "cash")
            {
                this.payWithCash(item);
            } else {
                this.payWithCreditCard(item);
            }
        }
    },
    backspaceBuffer: function(){
        this.buffer = this.buffer.substring(0, this.buffer.length - 1);
        $("#Buffer").html(this.buffer);
    },
    onPaymentTypePress: function(paymentType){
        if (paymentType === "cash")
        {
            $("#creditCardPanel").hide();
            $("#currenciesPanel").show();
        } else {
            $("#currenciesPanel").hide();
            $("#creditCardPanel").show();
        }
        this.resetState();
    },
    onKeypadPress: function (action) {
        this.lastAction = action;
        if(0 <= action && action <= 9) {
            VendingMachine.addToBuffer(action);
        } else if (action.charAt(0) === "$") {
            VendingMachine.addMoney(action);
        } else if (action === "Clear") {
            VendingMachine.returnMoney();
        } else if (action === "Enter") {
            VendingMachine.purchaseItem();
        } else if (action === "Delete") {
            VendingMachine.backspaceBuffer();
        } else {
            console.error("Undefined handler.");
        }
    },
    getItemById: function (id) {
        return ($.grep(this.products, function (product) {
            return product.Id === id;
        }))[0];
    }
};

function formatCurrency(price)
{
    return "$" + price.toFixed(2);
}


String.prototype.row = function ()
{
    return "<tr>" + this + "</tr>";
}

String.prototype.th = function (attrs) {
    return "<th>" + this + "</th>";
}

String.prototype.cell = function (attrs) {
    return "<td>" + this + "</td>";
}

$(document).ready(function () {
    $("#Keypad button").on("click", function (e) {
        VendingMachine.onKeypadPress(e.target.innerHTML);
    });

    $("#ResetProducts").on("click", function (e) {
        VendingMachine.resetProducts();
    });

    $('input[type="radio"]').on("click", function (e) {
        VendingMachine.onPaymentTypePress(e.target.value);
    });
    VendingMachine.loadProducts();
});