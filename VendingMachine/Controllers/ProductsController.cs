﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VendingMachine.Models;
using VendingMachine.Repositories;

namespace VendingMachine.Controllers
{
    [RoutePrefix("api")]
    public class ProductsController : ApiController
    {
        public IEnumerable<Product> GetAllProducts()
        {
            var repo = new ProductRepository();
            int count = repo.GetProductCount();
            return repo.GetProducts(count);
       }

        [Route("reset")]
        [HttpGet]
        public HttpResponseMessage Reset()
        {
            var repo = new ProductRepository();
            if (repo.Reset())
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Msg = "Products were reset." });
            }
            return Request.CreateResponse(HttpStatusCode.OK, new { Msg = "Products were not reset." });
        }
        /*
        public IHttpActionResult GetProduct(int id)
        {
            var product = products.FirstOrDefault(p => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }
        */
    }
}
