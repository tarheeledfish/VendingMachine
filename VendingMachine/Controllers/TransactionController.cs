﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VendingMachine.Models;
using VendingMachine.Repositories;
using VendingMachine.Services;

namespace VendingMachine.Controllers
{
    [RoutePrefix("api")]
    public class TransactionController : ApiController
    {
        HttpResponseMessage ProcessCash(Transaction transaction)
        {
            var repo = new ProductRepository();
            if (repo.SellOneProduct(transaction.Id))
            {
                return Request.CreateResponse(HttpStatusCode.OK,
                    new
                    {
                        Msg = "Thank you for your purchase!",
                        Change = 0
                    });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK,
                    new
                    {
                        Msg = "Not enough quantity of this product for this cash purchase.",
                    });
            }
        }

        HttpResponseMessage ProcessCreditCard(Transaction transaction)
        {
            if (CreditCardService.CardIsValid(transaction.CcNumber))
            {
                var repo = new ProductRepository();
                if (repo.SellOneProduct(transaction.Id))
                {
                    decimal chargeAmount = Math.Round(repo.GetProduct(transaction.Id).Price * 1.05M, 2);
                    return Request.CreateResponse(HttpStatusCode.OK,
                        new
                        {
                            Msg = "Your card was successfully charged $" + chargeAmount + "."
                        });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        new
                        {
                            Msg = "Not enough quantity of this product for this credit card purchase.",
                        });
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK,
                    new
                    {
                        Msg = "Invalid credit card number."
                    });
            }
        }

        //IHttpActionResult
        [Route("products/{productId}/buy")]
        [HttpPost]
        public HttpResponseMessage Post([FromBody]Transaction transaction)
        {
            HttpResponseMessage msg;
            if (transaction.PaymentType.Equals("cash"))
            {
                msg = ProcessCash(transaction);
            }
            else if (transaction.PaymentType.Equals("creditCard"))
            {
                msg = ProcessCreditCard(transaction);
            }
            else
            {
                msg = Request.CreateResponse(HttpStatusCode.OK, new { Msg = "Invalid Payment Type." });
            }
            return msg;
        }
    }
}
