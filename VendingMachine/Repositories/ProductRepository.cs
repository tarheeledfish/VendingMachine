﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Web;
using VendingMachine.Models;

namespace VendingMachine.Repositories
{
    public class ProductRepository
    {
        static SqlConnection _sqlConn = null;
        static string _connectionString = 
            System.Configuration.ConfigurationManager.ConnectionStrings["VendingMachine"].ConnectionString;

        public ProductRepository()
        {
        }

        public int GetProductCount()
        {
            int count = 0;
            using (_sqlConn = new SqlConnection(_connectionString))
            {
                _sqlConn.Open();
                using (SqlCommand command = new SqlCommand("select count(*) from Product", _sqlConn))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    reader.Read();
                    count = reader.GetInt32(0);
                }
                _sqlConn.Close();
            }
            return count;
        }

        public Product GetProduct(int Id)
        {
            Product product = new Product();
            using (_sqlConn = new SqlConnection(_connectionString))
            {
                _sqlConn.Open();

                SqlCommand command = new SqlCommand("select * from Product where Id = @Id", _sqlConn);
                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@Id";
                parameter.Value = Id;
                command.Parameters.Add(parameter);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    reader.Read();
                    product.Id = reader.GetInt32(0);
                    product.Price = reader.GetDecimal(1);
                    product.Name = reader.GetString(2);
                    product.Quantity = reader.GetByte(3);
                }
                _sqlConn.Close();
            }
            return product;
        }

        public Product[] GetProducts(int count)
        {
            Product[] products = new Product[count];
            using (_sqlConn = new SqlConnection(_connectionString))
            {
                _sqlConn.Open();
                using (SqlCommand command = new SqlCommand("select * from Product", _sqlConn))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    int i = 0;
                    while (reader.Read())
                    {
                        products[i++] = new Product
                        {
                            Id = reader.GetInt32(0),
                            Price = reader.GetDecimal(1),
                            Name = reader.GetString(2),
                            Quantity = reader.GetByte(3)
                        };
                    }
                }
                _sqlConn.Close();
            }
            return products;
        }

        internal int GetQuantity(int Id)
        {
            int quantity = 0;
            using (_sqlConn = new SqlConnection(_connectionString))
            {
                _sqlConn.Open();

                SqlCommand command = new SqlCommand("select Quantity from Product where Id = @Id", _sqlConn);
                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@Id";
                parameter.Value = Id;
                command.Parameters.Add(parameter);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    reader.Read();
                    quantity = reader.GetByte(0);
                }
                _sqlConn.Close();
            }
            return quantity;
        }

        internal bool SellOneProduct(int Id)
        {
            if(GetQuantity(Id) > 0)
            {
                Product product = new Product();
                using (_sqlConn = new SqlConnection(_connectionString))
                {
                    _sqlConn.Open();

                    SqlCommand command = new SqlCommand("update Product set Quantity = Quantity - 1 where Id = @Id", _sqlConn);
                    SqlParameter parameter = new SqlParameter();
                    parameter.ParameterName = "@Id";
                    parameter.Value = Id;
                    command.Parameters.Add(parameter);
                    var result = command.ExecuteScalar();
                    _sqlConn.Close();
                }
                return true;
            }
            return false;
        }

        internal bool Reset()
        {
            using (_sqlConn = new SqlConnection(_connectionString))
            {
                _sqlConn.Open();

                SqlCommand command = new SqlCommand("update Product set Quantity = 5", _sqlConn);
                var result = command.ExecuteScalar();
                _sqlConn.Close();
                return true;
            }
        }
    }
}