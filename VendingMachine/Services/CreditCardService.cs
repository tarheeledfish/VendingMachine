﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace VendingMachine.Services
{
    public static class CreditCardService
    {
        public static bool CardIsValid(string CcNumber)
        {
            return Regex.IsMatch(CcNumber, "^[0-9]{16}$");
        }
    }
}