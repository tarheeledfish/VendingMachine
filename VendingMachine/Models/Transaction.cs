﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendingMachine.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public decimal Money { get; set; }
        public string CcNumber { get; set; }
        public string PaymentType { get; set; }

        public Transaction()
        {

        }
    }
}